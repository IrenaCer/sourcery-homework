function add(a,b) {
    return a + b;
}

function mul(a,b) {
    return a * b;
}

function identityf(a) {
    function identity() {
        return a;
    }
    return identity;
}
function addf(a) {
    return function(b) {
        return a + b;
    }
}

function curry(foo, a) {
    return function(b) {
        return foo(a,b);
    }
}

function inc1(a) {
    return add(a, 1);
}

function inc2(a) {
    return addf(a)(1);
}

function inc3(a) {
    return curry(add, a)(1);
}

function double(foo) {
    return function(a) {
        return foo(a, a);
    }
}
